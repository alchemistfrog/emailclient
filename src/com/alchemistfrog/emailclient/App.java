package com.alchemistfrog.emailclient;

import com.alchemistfrog.emailclient.view.ViewFactory;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author Romain Ducreux <rducreux@pm.me>
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {
        ViewFactory viewFactory = new ViewFactory(new EmailManager());
        viewFactory.showOptionsWindow();
        viewFactory.updateStages();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
