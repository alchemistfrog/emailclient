package com.alchemistfrog.emailclient.view;

/**
 *
 * @author Romain Ducreux <rducreux@pm.me>
 */
public enum ColorTheme {
    LIGHT,
    DEFAULT,
    DARK;

    public static String getCssPath(ColorTheme colorTheme) {
        switch (colorTheme) {
            case LIGHT:
                return "css/themeLight.css";
            case DARK:
                return "css/themeDark.css";
            default:
                return "css/themeDefault.css";
        }
    }
}
