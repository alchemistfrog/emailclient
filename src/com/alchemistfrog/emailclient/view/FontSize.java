package com.alchemistfrog.emailclient.view;

/**
 *
 * @author Romain Ducreux <rducreux@pm.me>
 */
public enum FontSize {
    SMALL,
    MEDIUM,
    BIG;

    public static String getCssPath(FontSize fontSize) {
        switch (fontSize) {
            case SMALL:
                return "css/fontSmall.css";
            case BIG:
                return "css/fontBig.css";
            default:
                return "css/fontMedium.css";
        }
    }
}
