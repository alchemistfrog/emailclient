package com.alchemistfrog.emailclient.view;

import com.alchemistfrog.emailclient.EmailManager;
import com.alchemistfrog.emailclient.controller.BaseController;
import com.alchemistfrog.emailclient.controller.LoginWindowController;
import com.alchemistfrog.emailclient.controller.MainWindowController;
import com.alchemistfrog.emailclient.controller.OptionsWindowController;
import java.io.IOException;
import java.util.ArrayList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Romain Ducreux <rducreux@pm.me>
 */
public class ViewFactory {

    private EmailManager emailManager;
    private ArrayList<Stage> activeStages;
    private ColorTheme colorTheme = ColorTheme.DARK;
    private FontSize fontSize = FontSize.MEDIUM;

    public ViewFactory(EmailManager emailManager) {
        this.emailManager = emailManager;
        this.activeStages = new ArrayList<>();
    }

    public void showLoginWindow() {
        BaseController controller = new LoginWindowController(emailManager, this, "LoginWindow.fxml");
        this.initializeStage(controller);
    }

    public void showMainWindow() {
        BaseController controller = new MainWindowController(emailManager, this, "MainWindow.fxml");
        this.initializeStage(controller);
    }

    public void showOptionsWindow() {
        BaseController controller = new OptionsWindowController(emailManager, this, "OptionsWindow.fxml");
        this.initializeStage(controller);
    }

    public void closeStage(Stage stageToClose) {
        stageToClose.close();
        activeStages.remove(stageToClose);
    }

    private void initializeStage(BaseController baseController) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(baseController.getFxmlName()));
        fxmlLoader.setController(baseController);
        Parent parent;

        try {
            parent = fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        Scene scene = new Scene(parent);
        Stage stage = new Stage();

        baseController.setStage(stage);

        stage.setScene(scene);
        stage.show();
        activeStages.add(stage);
    }

    public EmailManager getEmailManager() {
        return emailManager;
    }

    public void setEmailManager(EmailManager emailManager) {
        this.emailManager = emailManager;
    }

    public FontSize getFontSize() {
        return fontSize;
    }

    public void setFontSize(FontSize fontSize) {
        this.fontSize = fontSize;
    }

    public ColorTheme getColorTheme() {
        return colorTheme;
    }

    public void setColorTheme(ColorTheme colorTheme) {
        this.colorTheme = colorTheme;
    }

    public void updateStages() {
        for (Stage stage : activeStages) {
            Scene scene = stage.getScene();
            String themePath = ColorTheme.getCssPath(colorTheme);
            String fontSizePath = FontSize.getCssPath(fontSize);

            System.out.println(themePath);
            System.out.println(colorTheme);
            System.out.println(fontSizePath);
            System.out.println(fontSize);

            scene.getStylesheets().clear();
            scene.getStylesheets().add(getClass().getResource(themePath).toExternalForm());
            scene.getStylesheets().add(getClass().getResource(fontSizePath).toExternalForm());
        }
    }

}
