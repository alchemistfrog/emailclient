package com.alchemistfrog.emailclient.controller;

import com.alchemistfrog.emailclient.EmailManager;
import com.alchemistfrog.emailclient.view.ViewFactory;
import javafx.stage.Stage;

/**
 *
 * @author Romain Ducreux <rducreux@pm.me>
 */
public abstract class BaseController {

    private EmailManager emailManager;
    protected ViewFactory viewFactory;
    private String fxmlName;
    private Stage stage;

    public BaseController(EmailManager emailManager, ViewFactory viewFactory, String fxmlName) {
        this.emailManager = emailManager;
        this.viewFactory = viewFactory;
        this.fxmlName = fxmlName;
    }

    public String getFxmlName() {
        return fxmlName;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

}
