package com.alchemistfrog.emailclient.controller;

import com.alchemistfrog.emailclient.EmailManager;
import com.alchemistfrog.emailclient.view.ViewFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Romain Ducreux <rducreux@pm.me>
 */
public class LoginWindowController extends BaseController {

    @FXML
    private TextField emailAddressField;

    @FXML
    private TextField passwordField;

    public LoginWindowController(EmailManager emailManager, ViewFactory viewFactory, String fxmlName) {
        super(emailManager, viewFactory, fxmlName);
    }

    @FXML
    public void loginAction(ActionEvent event) {
        System.out.println("Login...");
        this.viewFactory.showMainWindow();
        this.viewFactory.closeStage(this.getStage());
    }

    @FXML
    public void showOptionsAction(ActionEvent event) {
        viewFactory.showLoginWindow();
    }

}
