module com.alchemistfrog.emailclient {
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.web;
    requires java.activation;
    requires java.mail;

    opens com.alchemistfrog.emailclient;
    opens com.alchemistfrog.emailclient.view;
    opens com.alchemistfrog.emailclient.controller;
}
